// find_pi.cpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-05-03 20:49:54 (jonah)>

// This little piece of code calculates pi just once but it runs much
// faster than the python code.

// uses the C++11 standard.

// ----------------------------------------------------------------------

// Include
#include <iostream>
#include <random>
#include <cmath>
using namespace std;

const int NUM_ITERATIONS = 1000000000; // number of iterations the algorithm goes through
const double RADIUS = 1.0; // radius of the circle

int main() {
  // x and y coordinates
  double x;
  double y;

  // counts
  double square_count = 0; // number of darts 
  double circle_count = 0; // number of darts in the circle

  // and our estimate for pi
  double pi_estimate;
  // and the error
  double pi_error;

  cout << "Calculating pi using " << NUM_ITERATIONS << " darts." << endl;
  
  // initialize random number generator
  default_random_engine generator;
  uniform_real_distribution<double> distribution(-RADIUS,RADIUS);
  for (int i = 0; i < NUM_ITERATIONS; i++) {
    x = distribution(generator);
    y = distribution(generator);
    square_count += 1;
    if ( (x*x + y*y) <= RADIUS ) {
      circle_count += 1;
    }
  }
  cout << "Number of darts in the circle is: " << circle_count << "." << endl;
  cout << "Number of darts in the square is: " << square_count << "." << endl;
  pi_estimate = 4.0*(circle_count/square_count);
  pi_error = abs(M_PI - pi_estimate);
  cout << "Our estimate for pi is: " << pi_estimate << "." << endl;
  cout << "This gives an error of: " << pi_error << "." << endl;

  return 0;
}
  
