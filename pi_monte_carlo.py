#!/usr/bin/env python2

"""
pi_monte_carlo.py

Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Website: http://www.thephysicsmill.com
Time-stamp: <2016-05-09 10:12:28 (jmiller)>

This little script makes an animation of a monte carlo simulation to
calculate the value of Pi.
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.animation as animation
import random
# ----------------------------------------------------------------------


# Important constants
# ----------------------------------------------------------------------
# Animation parameters
radius = 1 # Radius of the circle and square
movie_time = 20 # seconds
num_frames = 100 # Number of "darts" we throw at our "board"
min_fps = 30
movie_fps = max(num_frames/movie_time,min_fps)
animation_filename = "monte_carlo_for_pi"
animation_format = 'mp4'

# Rectangle and circle plotting data
rectangle_xmin = -radius
rectangle_ymin = rectangle_xmin
rectangle_width = 2*radius
rectangle_height = rectangle_width
rectangle_xmax = rectangle_xmin + rectangle_width
rectangle_ymax = rectangle_ymin + rectangle_height
circle_center = (0,0)

# plotrange
xmin = 1.3*rectangle_xmin
xmax = - xmin
ymax = xmax
ymin = -ymax

# Text data
text_x = 0.5*xmax # x coordinate for the text to start
circle_text_x = 0.5*xmax
total_counter_y = 0.94*ymax # Where the counter for the total number of
                           # darts starts. y-coordinate
circle_counter_y = 0.88*ymax # Where the counter for the number of
                            # darts within the circle
                            # starts. y-coordinate
pi_estimate_y = 0.81*ymax # Where we start the test for our estimate of
                         # pi. y-coordinate.
counter_text = "#darts = {}"
circle_text =  "#darts in circle = {}"
pi_estimate_text = r'$\pi\approx$' + "{}"
# ----------------------------------------------------------------------

def estimate_pi(square_count,circle_count):
    """
    The area of the circle divided by the area of the square is pi/4.
    """
    if square_count != 0:
        return 4*float(circle_count)/float(square_count)
    else:
        return 0

def monte_carlo_for_pi(num_random_samples):
    """
    Calculates the value for pi by monte carlo algorithm. Generates
    data that can be animated.
    """
    # We also calculate the total counts ahead of time
    total_counts = [i for i in range(num_random_samples)] # total number of "darts"
    circle_counts = [0 for i in range(num_random_samples)] # darts in circle
    current_circle_count = 0

    # Generate a random sample
    random.seed()
    # Note: the following syntax would work too, but I decided against
    # it because the order of the samples is wrong. Since the samples
    # are random this shouldn't matter, but let's do it right.

    # sampled_x_coords = [random.uniform(rectangle_xmin,rectangle_xmax)\
    #                         for i in range(num_random_samples)]
    # sampled_y_coords = [random.uniform(rectangle_ymin,rectangle_ymax)\
    #                         for i in range(num_random_samples)]

    # That's why I went with this syntax
    sampled_x_coords = [0.0 for i in range(num_random_samples)]
    sampled_y_coords = [0.0 for i in range(num_random_samples)]
    for i in range(num_random_samples):
        sampled_x_coords[i] = random.uniform(rectangle_xmin,rectangle_xmax)
        sampled_y_coords[i] = random.uniform(rectangle_ymin,rectangle_ymax)
        if sampled_x_coords[i]**2 + sampled_y_coords[i]**2 <= radius:
            current_circle_count += 1
        circle_counts[i] = current_circle_count

    # And estimate pi for each time step
    pi_estimates = [estimate_pi(total_counts[i],circle_counts[i],)\
                        for i in range(num_random_samples)]

    return sampled_x_coords,sampled_y_coords,total_counts,circle_counts,pi_estimates
  
def animate_pi(sampled_x_coords,
               sampled_y_coords,
               total_counts,
               circle_counts,
               pi_estimates):
    """
    Animates the calculation of pi.
    """
   
    # define plot objects
    fig = plt.figure()
    ax = plt.axes(xlim=(xmin,xmax),ylim=(ymin,ymax))
    scatter, = ax.plot([],[],'ro')
    total_counter=ax.text(text_x,total_counter_y,'')
    circle_counter=ax.text(circle_text_x,circle_counter_y,'')
    pi_estimator = ax.text(text_x,pi_estimate_y,'')
    rectangle = patches.Rectangle((rectangle_xmin,rectangle_ymin),
                                  rectangle_width,rectangle_height,
                                  fc='none', ec='none',linewidth=2)
    circle = patches.Circle(circle_center,radius,
                            fc='none',ec='none',linewidth=2)
    ax.add_patch(rectangle)
    ax.add_patch(circle)
    ax.set_axis_off()
    
    # This draws the first frame and returns the objects to be updated
    # each frame.
    def init():
        scatter.set_data([],[])
        total_counter.set_text('')
        circle_counter.set_text('')
        pi_estimator.set_text('')
        rectangle.set_edgecolor('none')
        circle.set_edgecolor('none')
        return scatter,total_counter,circle_counter,pi_estimator,rectangle,circle

    # This updates each new frame
    def animate(i):
        scatter.set_data(sampled_x_coords[:i+1],sampled_y_coords[:i+1])
        total_counter.set_text(counter_text.format(total_counts[i]))
        circle_counter.set_text(circle_text.format(circle_counts[i]))
        pi_estimator.set_text(pi_estimate_text.format(pi_estimates[i]))
        rectangle.set_edgecolor('b')
        circle.set_edgecolor('b')
        return scatter,total_counter,circle_counter,pi_estimator,rectangle,circle

    anim = animation.FuncAnimation(fig,animate,
                                   frames=total_counts,
                                   init_func=init,blit=True)
    
    anim.save(animation_filename+"."+animation_format,
              fps=movie_fps)

    #plt.show()
    return

def main():
    sampled_x_coords,sampled_y_coords,total_counts,circle_counts,pi_estimates\
        =monte_carlo_for_pi(num_frames)
    animate_pi(sampled_x_coords,
               sampled_y_coords,
               total_counts,circle_counts,
               pi_estimates)
    return

if __name__ == "__main__":
    main()

