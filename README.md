pi_monte_carlo

We include two programs to calculate pi by monte carlo simulation:
    The python script generates an animation but is slower.
    The c++ program calculates pi once but is much faster.
An explanation can be found here:
http://www.thephysicsmill.com/2014/05/03/throwing-darts-pi/

Dependancies:
    The gnu c++11 compiler.
    python2
    numpu
    scipy
    matplotlib
    ffmpeg (for saving the animation)

To run the c++ program, compile it with the following command in the terminal:
   g++ -Wall -std=c++11 -o find_pi.exe find_pi.cpp
and simply run the resulting binary.

To run the python code, simply type the following command in the terminal:
    python2 pi_monte_carlo.py
    